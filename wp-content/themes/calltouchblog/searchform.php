<form  class="header__search" role="search" method="get" id="searchform" action="<?php echo home_url( '/' ) ?>" >
	<span class="header__search-btn"></span>
	<div class="header__search-form">
		<?php //get_search_form(); ?>
		<input type="search" class="autocomplete-input" value="<?php echo get_search_query() ?>" name="s" id="s" >
		<div class="header__search-close"></div>
	</div>
</form>