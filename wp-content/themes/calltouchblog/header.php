<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Calltouch-blog
 */

?><!DOCTYPE html>
<html  <?php language_attributes(); ?> prefix="og: http://ogp.me/ns#">
<head>
	<!-- Pages tittle ============================================================================ -->
	<title><?php wp_title( '|', true, 'right' );?></title>
	<!-- Necessary pages settings ================================================================ -->
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width; initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php wp_head(); ?>
<style>
header,main,footer{display: none;}
</style>
<script type="text/javascript"> (function (w, d, nv, ls, yac){ var lwait = function (w, on, trf, dly, ma, orf, osf) { var pfx = "ct_await_", sfx = "_completed"; if(!w[pfx + on + sfx]) { var ci = clearInterval, si = setInterval, st = setTimeout , cmld = function () { if (!w[pfx + on + sfx]) { w[pfx + on + sfx] = true; if ((w[pfx + on] && (w[pfx + on].timer))) { ci(w[pfx + on].timer); w[pfx + on] = null; } orf(w[on]); } };if (!w[on] || !osf) { if (trf(w[on])) { cmld(); } else { if (!w[pfx + on]) { w[pfx + on] = { timer: si(function () { if (trf(w[on]) || ma < ++w[pfx + on].attempt) { cmld(); } }, dly), attempt: 0 }; } } } else { if (trf(w[on])) { cmld(); } else { osf(cmld); st(function () { lwait(w, on, trf, dly, ma, orf); }, 0); } }}}; var ct = function (w, d, e, c, n){ var a = 'all', b = 'tou', src = b + 'c' + 'h'; src = 'm' + 'o' + 'd.c' + a + src; var jsHost = "https://" + src, p = d.getElementsByTagName(e)[0], s = [{"sp":"3","sc":d.createElement(e)}]; var jsf = function (w, d, p, s, h, c, n, yc) { if (yc !== null) { lwait(w, 'yaCounter'+yc, function(obj) { return (obj && obj.getClientID ? true : false); }, 50, 100, function(yaCounter) { s.forEach(function(e) { e.sc.async = 1; e.sc.src = jsHost + "." + "r" + "u/d_client.js?param;specific_id"+e.sp+";" + (yaCounter && yaCounter.getClientID ? "ya_client_id" + yaCounter.getClientID() + ";" : "") + (c ? "client_id" + c + ";" : "") + "ref" + escape(d.referrer) + ";url" + escape(d.URL) + ";cook" + escape(d.cookie) + ";attrs" + escape("{\"attrh\":" + n + ",\"ver\":170523}") + ";"; p.parentNode.insertBefore(e.sc, p); }); }, function (f) { if(w.jQuery) { w.jQuery(d).on('yacounter' + yc + 'inited', f ); }}); } else { s.forEach(function(e) { e.sc.async = 1; e.sc.src = jsHost + "." + "r" + "u/d_client.js?param;specific_id"+e.sp+";" + (c ? "client_id" + c + ";" : "") + "ref" + escape(d.referrer) + ";url" + escape(d.URL) + ";cook" + escape(d.cookie) + ";attrs" + escape("{\"attrh\":" + n + ",\"ver\":170523}") + ";"; p.parentNode.insertBefore(e.sc, p); }); }}; if (!w.jQuery) { var jq = d.createElement(e); jq.src = jsHost + "." + "r" + 'u/js/jquery-1.7.min.js'; jq.onload = function () { lwait(w, 'jQuery', function(obj) { return (obj ? true : false); }, 30, 100, function () { jsf(w, d, d.getElementsByTagName(e)[0], s, jsHost, c, n, yac); }); }; p.parentNode.insertBefore(jq, p); } else { jsf(w, d, p, s, jsHost, c, n, yac); }}; var gaid = function (w, d, o, ct, n) { if (!!o) { lwait(w, o, function (obj) { return (obj && obj.getAll ? true : false); }, 200, (nv.userAgent.match(/Opera|OPR\//) ? 10 : 20), function (gaCounter) { var clId = null; try { var cnt = gaCounter && gaCounter.getAll ? gaCounter.getAll() : null; clId = cnt && cnt.length > 0 && !!cnt[0] && cnt[0].get ? cnt[0].get('clientId') : null; } catch (e) { console.warn("Unable to get clientId, Error: " + e.message); } ct(w, d, 'script', clId, n); }, function (f) { w[o](function () { f(w[o]); })});} else{ ct(w, d, 'script', null, n); }}; var cid = function () { try { var m1 = d.cookie.match('(?:^|;)\\s*_ga=([^;]*)');if (!(m1 && m1.length > 1)) return null; var m2 = decodeURIComponent(m1[1]).match(/(\d+\.\d+)$/); if (!(m2 && m2.length > 1)) return null; return m2[1]} catch (err) {}}(); if (cid === null && !!w.GoogleAnalyticsObject){ if (w.GoogleAnalyticsObject=='ga_ckpr') w.ct_ga='ga'; else w.ct_ga = w.GoogleAnalyticsObject; if (typeof Promise !== "undefined" && Promise.toString().indexOf("[native code]") !== -1){new Promise(function (resolve) {var db, on = function () { resolve(true) }, off = function () { resolve(false)}, tryls = function tryls() { try { ls && ls.length ? off() : (ls.x = 1, ls.removeItem("x"), off());} catch (e) { nv.cookieEnabled ? on() : off(); }};w.webkitRequestFileSystem ? webkitRequestFileSystem(0, 0, off, on) : "MozAppearance" in d.documentElement.style ? (db = indexedDB.open("test"), db.onerror = on, db.onsuccess = off) : /constructor/i.test(w.HTMLElement) ? tryls() : !w.indexedDB && (w.PointerEvent || w.MSPointerEvent) ? on() : off();}).then(function (pm){ if (pm){gaid(w, d, w.ct_ga, ct, 2);}else{gaid(w, d, w.ct_ga, ct, 3);}})}else{gaid(w, d, w.ct_ga, ct, 4);} }else{ct(w, d, 'script', cid, 1);}}) (window, document, navigator, localStorage, "46298718"); </script>

</head>

<body <?php body_class();?> >
	<!-- HEADER -->
	<header class="header">
		<div class="container">
			<a href="https://www.calltouch.ru" class="logo" rel="home" style="background: url(/wp-content/themes/calltouch-blog/assets/img/call-touch-logo.svg) no-repeat;"></a>
			
			<div class="header__top">
				<!-- <div class="header__lang">
					<ul class="header__list">
						<li class="active"><a href="#">РУС</a></li>
						<li><a href="#">ENG</a></li>
					</ul>
				</div> -->
				<div class="header__contacts">
					<p>Есть вопросы? Звоните:</p>
					<ul class="header__list call_phone_1_1">
						<!-- <li><a href="tel:+7 495 989-47-68">+7 495 989-47-68</a></li>
						<li><a href="tel:8 800 989-47-68">8 800 989-47-68</a></li> -->
						<li><a href="tel:+74952415811">+7 495 241-58-11</a></li>
						<li><a href="tel:88002221381">8 800 222-13-81</a></li>
					</ul>
				</div>
			</div>


			<div class="header__bottom">
				<div class="header__buttons">
                    <a href="https://lk.calltouch.ru/login" class="btn blue shadow" target="_blank">Войти</a>
                    <a href="https://lk.calltouch.ru/registration" class="btn yellow shadow" target="_blank">Подключиться</a>
				</div>
				<?php get_search_form()?>
				<div class="header__phone">
					<ul class="header__list call_phone_1_1">
					<li><a href="tel:+74952415811">+7 495 241-58-11</a></li>
					<li><a href="tel:88002221381">8 800 222-13-81</a></li>        </ul>
					<img class="js-phone_panel" src="/wp-content/themes/calltouch-blog/assets/img/phone.svg" alt="">
				</div>
				<!-- <nav class="nav">
					<ul>
						<li class="nav__list-item nav__has-submenu">
							<a href="#">Продукт</a>
							<div class="nav__submenu">
								<ul class="nav__submenu__list">
            						<li class="nav__submenu__caption">Технологии</li>
                                    <li class="nav__submenu__list-item">
				                        <a href="https://www.calltouch.ru/product/analiticheskaya-platforma/">Аналитическая платформа</a>
				                    </li>
                                    <li class="nav__submenu__list-item">
				                        <a href="https://www.calltouch.ru/product/calltracking/">Коллтрекинг</a>
				                    </li>
                                    <li class="nav__submenu__list-item">
				                        <a href="https://www.calltouch.ru/product/optimizator/">Оптимизатор</a>
				                    </li>
                                    <li class="nav__submenu__list-item">
				                        <a href="https://www.calltouch.ru/product/callback/">Обратный звонок</a>
				                    </li>
                                    <li class="nav__submenu__list-item">
				                        <a href="https://www.calltouch.ru/product/predict/">Predict</a>
				                    </li>
				                    <li class="nav__submenu__list-item">
				                        <a href="https://www.calltouch.ru/product/antifrod/">Антифрод</a>
				                    </li>

                                </ul>
								<ul class="nav__submenu__list">
            						<li class="nav__submenu__caption">Применение</li>
                                    	<li class="nav__submenu__list-item">
							                <a href="https://www.calltouch.ru/product/skvoznaya-analitika/">Сквозная аналитика</a>
							            </li>
                                        <li class="nav__submenu__list-item">
					                        <a href="https://www.calltouch.ru/product/kontekstnaya-reklama/">Контекстная реклама</a>
					                    </li>
                                        <li class="nav__submenu__list-item">
					                        <a href="https://www.calltouch.ru/product/kontrol-kachestva/">Контроль качества</a>
					                    </li>
                                        <li class="nav__submenu__list-item">
					                        <a href="https://www.calltouch.ru/product/lidogeneratsiya/">Лидогенерация</a>
					                    </li>
                                </ul>
								<ul class="nav__submenu__list">
            						<li class="nav__submenu__caption">Интеграции</li>
                                    <li class="nav__submenu__list-item">
				                        <a href="https://www.calltouch.ru/product/sistemy-analitiki/">Системы аналитики</a>
				                    </li>
                                    <li class="nav__submenu__list-item">
				                        <a href="https://www.calltouch.ru/product/reklamnye-ploshchadki/">Рекламные площадки</a>
				                    </li>
                                    <li class="nav__submenu__list-item">
				                        <a href="https://www.calltouch.ru/product/crm/">CRM</a>
				                    </li>
                                    <li class="nav__submenu__list-item">
				                        <a href="https://www.calltouch.ru/product/optimizatory-konteksta/">Оптимизаторы контекста</a>
				                    </li>
                                    <li class="nav__submenu__list-item">
				                        <a href="https://www.calltouch.ru/product/lidogeneratory/">Лидогенераторы</a>
				                    </li>
                                </ul>
							</div>
						</li>
						<li class="nav__list-item">
							<a href="https://www.calltouch.ru/pricing/">Цены</a>
						</li>
						<li class="nav__list-item nav__has-submenu">
							<a href="https://www.calltouch.ru/partners/">Партнерам</a>
                            <div class="nav__submenu">
                        		<ul class="nav__submenu__list">
                                    <li class="nav__submenu__list-item">
                                    	<a href="https://www.calltouch.ru/partners/">Партнерам</a>
                                	</li>
                                    <li class="nav__submenu__list-item">
                                    	<a href="https://www.calltouch.ru/partners/agency/">Сертифицированные агентства</a>
                                	</li>
                                    <li class="nav__submenu__list-item">
                                    	<a href="https://www.calltouch.ru/partners/certification/">Сертификация</a>
                                	</li>
                                    <li class="nav__submenu__list-item">
                                    	<a href="https://www.calltouch.ru/partners/education/">Обучение</a>
                                	</li>
                                </ul>
                   			</div>
                        </li>
						<li class="nav__list-item nav__has-submenu">
                			<a >Академия</a>
                        	<div class="nav__submenu">
                        		<ul class="nav__submenu__list">
                                	<li class="nav__submenu__list-item">
                                    	<a href="https://blog.calltouch.ru/category/casestudies/">Кейсы и статьи</a>
                                	</li>
                                	<li class="nav__submenu__list-item">
                                    	<a href="https://www.calltouch.ru/academy/events/">Мероприятия</a>
                                	</li>
                                    <li class="nav__submenu__list-item">
                                    	<a href="https://www.calltouch.ru/academy/video-tutorials/">Видеоуроки</a>
                                	</li>
                                    <li class="nav__submenu__list-item">
                                    	<a href="https://www.calltouch.ru/academy/events/webinar/">Вебинары</a>
                                	</li>
                                    <li class="nav__submenu__list-item">
                                    	<a href="https://support.calltouch.ru/" target="_blank">Справочный центр</a>
                                	</li>
                                </ul>
                			</div>
                        </li>
                        <li class="nav__list-item nav__has-submenu">
                        	<a href="https://www.calltouch.ru/about/">О компании</a>
                           	<div class="nav__submenu">
                        		<ul class="nav__submenu__list">
                                    <li class="nav__submenu__list-item">
                                    	<a href="https://www.calltouch.ru/about/press-list/">Пресса о нас</a>
                                	</li><li class="nav__submenu__list-item">
                                    	<a href="https://www.calltouch.ru/about/vacancies/">Вакансии</a>
                                	</li>
                                    <li class="nav__submenu__list-item">
                                    	<a href="https://www.calltouch.ru/about/contacts/">Контакты</a>
                                	</li>
                                </ul>
                    		</div>
                        </li>
						<li class="nav__list-item active">
                			<a href="/">Блог</a>
                        </li>
					</ul>
				</nav> -->
			</div>
			<div class="menu-bugrer"></div>
			<div class="header__mobile"></div>
		</div>
	</header>
	<!-- /HEADER -->


	<!-- WRAPPER -->
	<main class="wrapper">
		<!-- PAGE -->

        <?php $arPage = explode("/", $_SERVER["REQUEST_URI"]);?>
		<div class="page <?php if(is_category() == false && count($arPage) > 2) { echo "detail"; }?>">
			<!-- PAGE-HEAD -->
			<div class="page__head">
				<div class="container">

					<!-- BREADCRUMBS & schema.org -->
					<?php if (function_exists('breadcrumbs')) breadcrumbs(); ?>
					<!-- /BREADCRUMBS -->

					<?php if(is_category()):?>
						<h1 class="page__head-title"><?=get_queried_object()->name?></h1>
					<?php else: ?>	

						<?php if(mb_strtolower(get_the_title()) == "home" || mb_strtolower(get_the_title()) == "главная"):?>
							<h1 class="page__head-title">Блог</h1>
						<?php elseif(is_404()): ?>	
						<?php elseif(is_search()):?>
							<h1 class="page__head-title">Результаты по запросу "<?=get_search_query()?>"</h1>
						<?php else: ?>
							<h1 class="page__head-title"><?=get_the_title()?></h1>
						<?php endif;?>	
					<?php endif;?>


					<?php if(is_category() || is_front_page()): ?>

						<?php 

							$thisCat = get_queried_object();

							if(isset($thisCat->cat_ID)){
								$catSlug = $thisCat->slug;
							}

							$categories = get_categories();
						?>


						<?php if(count($categories)): ?>

							<div class="events__head">
		                    	<ul class="b-events-list">

		                    	<?php if(is_front_page()):?>

		                    		<li class="b-events-list__item">
					            		<a class="events__nav-btn is-active">Все</a>
					        		</li>

		                    	<?php else: ?>
		                    		<li class="b-events-list__item">
					            		<a class="events__nav-btn" href="/">Все</a>
					        		</li>
		                    	<?php endif;?>	

		                    	

		                    	<?php foreach($categories as $category):?>

		                    		<?php if(isset($catSlug) && $catSlug == $category->slug):?>

			                 			<li class="b-events-list__item">
						            		<a class="events__nav-btn is-active"><?=$category->name?></a>
						        		</li>

		                    		<?php else: ?>
		                    			<li class="b-events-list__item">
					            			<a class="events__nav-btn" href="/category/<?=$category->slug?>/"><?=$category->name?></a>
					        			</li>

		                    		<?php endif;?>	

		                    	<?php endforeach;?>	

			                     </ul>
							</div>

						<?php endif;?>

					<?php endif;?>	


				</div>
			</div>
			<!-- /PAGE-HEAD -->




