import './lib.min.js';

$(document).ready(function() {
  function e(e, t) {
    var a = $(e),
      s = +a.width(),
      i = parseInt(a.find('.noUi-origin').css('left')),
      n = a.find('.noUi-tooltip');
    (t = t ? -t : 100 * -(i / s)),
      n.css({
        transform: 'translate(' + t + '%, 0)',
        '-webkit-transform': 'translate(' + t + '%, 0)'
      });
  }

  function t(e) {
    return String(e).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
  }

  function a(e, t, a, s, i) {
    $(e).click(function() {
      var e = $(this),
        n = $(t),
        o = $(s),
        r = $(i);
      n.length &&
        (e.addClass('is-loading'),
        $.ajax({
          type: 'POST',
          url: a,
          dataType: 'html',
          cache: !1,
          error: function() {
            console.log('Error loading more'), e.removeClass('is-loading');
          },
          success: function(t) {
            console.log('Success loading more'),
              n.append(t),
              setTimeout(function() {
                n.children('.just-loaded').removeClass('just-loaded'),
                  e.removeClass('is-loading'),
                  o.length && r.length && o.text($(i).length);
              }, 1e3);
          }
        }));
    });
  }

  var s = !1;
  $('.js-phone_panel').on('click', function() {
    $(this)
      .parent()
      .addClass('active'),
      s && clearTimeout(s),
      (s = setTimeout(function() {
        $('.header__phone.active').fadeOut(300, function() {
          $(this)
            .addClass('notrans')
            .removeClass('active')
            .fadeIn(150, function() {
              $(this).removeClass('notrans');
            });
        });
      }, 5e3));
  });
  var i = /Firefox/i.test(navigator.userAgent) ? 'DOMMouseScroll' : 'mousewheel';
  $(window).bind(i, function(e) {
    if ($(this).scrollTop() > $('.header').innerHeight()) {
      var t = window.event || e;
      t = t.originalEvent ? t.originalEvent : t;
      var a = t.detail ? -40 * t.detail : t.wheelDelta;
      a > 0
        ? $('.header')
            .addClass('fixed visible')
            .removeClass('hide')
        : $('.header')
            .removeClass('visible')
            .addClass('hide');
    } else $('.header').removeClass('fixed visible hide');
  }),
    $('.slider-main').on('init reInit afterChange', function(e, t, a, s) {
      var i = (a ? a : 0) + 1;
      $('.slider .slider-actions__count').html(
        '<span class="current-slide">' + i + '</span><sup class="total-slides">&#92;' + t.slideCount + '</sup>'
      );
    }),
    $('.slider-main').on('click', '.slick-slide', function(e) {
      e.stopPropagation();
      var t = $(this).data('slick-index');
      $('.slider-main').slick('slickCurrentSlide') !== t && $('.slider-main').slick('slickGoTo', t);
    }),
    $('.slider-main').slick({
      centerMode: !0,
      centerPadding: '0',
      slidesToShow: 3,
      draggable: !1,
      appendArrows: $('.slider-actions'),
      asNavFor: '.slider-text',
      responsive: [{ breakpoint: 767, settings: { centerMode: !1, slidesToShow: 1 } }]
    }),
    $('.slider-text').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: !1,
      fade: !0,
      asNavFor: '.slider-main'
    }),
    $('.slider-work').slick({
      slidesToShow: 4,
      slidesToScroll: 4,
      arrows: !1,
      dots: !1,
      responsive: [
        {
          breakpoint: 1025,
          settings: { slidesToShow: 2, slidesToScroll: 2, dots: !0 }
        },
        {
          breakpoint: 767,
          settings: { slidesToShow: 1, slidesToScroll: 1, dots: !0 }
        }
      ]
    }),
    $('.slider-partners').slick({
      speed: 300,
      infinite: !1,
      dots: !1,
      slidesToShow: 7,
      slidesToScroll: 1,
      arrows: !0,
      variableWidth: !0,
      appendArrows: $('.slider-partners__actions')
    }),
    $('.button-container .btn').click(function(e) {
      e.preventDefault();
      var t = $(this).attr('data-id');
      $('[' + t + ']').addClass('active'), $(this).hide();
    }),
    $('.header__search-btn').click(function() {
      $(this)
        .closest('.header__search')
        .addClass('active');
    }),
    $('.header__search-close').click(function() {
      $(this)
        .closest('.header__search')
        .removeClass('active');
    }),
    $('.menu-bugrer').click(function() {
      $(this).toggleClass('active'), $('.header__mobile').toggleClass('active'), $('body').toggleClass('overflow');
    }),
    $('.nav__has-submenu > a').click(function(e) {
      e.preventDefault(), $(this).toggleClass('active');
    }),
    $('.tabs__nav span').click(function() {
      var e = $(this).attr('data-tab-id');
      $('.tabs__nav li').removeClass('active'),
        $('.tabs__content-item')
          .hide()
          .removeClass('active-tab'),
        $(this)
          .parent()
          .addClass('active'),
        $('#' + e)
          .fadeIn()
          .addClass('active-tab'),
        $('.slider-work').slick('setPosition');
    }),
    $(window).resize(function() {
      var e = $('.nav'),
        t = $('.header__contacts'),
        a = $('.header__buttons'),
        s = $('.header__lang'),
        i = $('.header__mobile');
      $(this).width() >= 767 && $(this).width() <= 1024
        ? (s.detach().appendTo(i),
          e.detach().appendTo(i),
          t.detach().appendTo(i),
          $('.nav').jScrollPane({ autoReinitialise: !0 }))
        : $(this).width() < 767
        ? (s.detach().appendTo(i),
          e.detach().appendTo(i),
          a.appendTo(i),
          t.detach().appendTo(i),
          $('.nav')
            .jScrollPane({})
            .data('jsp')
            .destroy())
        : (e.appendTo($('.header__bottom')),
          a.prependTo($('.header__bottom')),
          s.appendTo($('.header__top')),
          t.appendTo($('.header__top')),
          $('.nav')
            .jScrollPane({})
            .data('jsp')
            .destroy());
    }),
    $(window).each(function() {
      $(this).trigger('resize');
    });
  var n = $('.autocomplete-input').autocomplete({
      source: ['Москва, Россия', 'Москва, Россия', 'Москва, Россия'],
      minLength: 0
    }),
    o = n.data('ui-autocomplete') || n.data('autocomplete');
  o &&
    (o._renderItem = function(e, t) {
      var a = String(t.value).replace(new RegExp(this.term, 'gi'), "<span class='ui-state-highlight'>$&</span>");
      return $('<li></li>')
        .data('item.autocomplete', t)
        .append(a)
        .appendTo(e);
    }),
    ($.fn.youtubeInit = function() {
      var e = $(this);
      e.each(function(e, t) {
        function a() {
          (s =
            '<iframe class="youtube" width="740" height="480" src="https://www.youtube.com/embed/' +
            n +
            '?rel=0&autoplay=1" frameborder="0" allowfullscreen></iframe>'),
            i.before(s),
            i.addClass('hide');
        }

        var s,
          i = $(t),
          n = i.data('youtube');
        return n ? (i.click(a), this) : !1;
      });
    }),
    $('.js-youtube-init').youtubeInit(),
    ($.fn.inputField = function() {
      var e = $(this).children('input');
      return (
        e.each(function(e, t) {
          var a = $(t);
          '' !== a.val().trim() && a.parent().addClass('has-text');
        }),
        e.focusin(function() {
          $(this)
            .parent()
            .addClass('has-text');
        }),
        e.focusout(function() {
          var e,
            t = $(this);
          e = setTimeout(function() {
            '' === t.val().trim() && (t.parent().removeClass('has-text'), clearTimeout(e));
          }, 100);
        }),
        this
      );
    }),
    $('.js-input-field').inputField();
  var r = document.getElementById('count-session-slider'),
    l = document.getElementById('count-session-input');
  r &&
    (noUiSlider.create(r, {
      tooltips: [!0],
      format: wNumb({ decimals: 0 }),
      pips: { mode: 'values', values: [0, 15e3], density: 50 },
      start: [7500],
      step: 100,
      range: { min: [0], max: [15e3] }
    }),
    r.noUiSlider.on('update', function(t, a) {
      $(l)
        .val(t[a])
        .change(),
        e(r);
    }),
    l.addEventListener('change', function() {
      r.noUiSlider.set(this.value);
    }),
    e(r, '50'));
  var d = document.getElementById('length-session-slider'),
    c = document.getElementById('length-session-input');
  d &&
    (noUiSlider.create(d, {
      tooltips: [!0],
      format: wNumb({ decimals: 0, thousand: ' ', postfix: ' мин.' }),
      pips: { mode: 'values', values: [0, 60], density: 50 },
      start: [30],
      step: 1,
      range: { min: [0], max: [60] }
    }),
    d.noUiSlider.on('update', function(t, a) {
      $(c)
        .val(t[a])
        .change(),
        e(d);
    }),
    c.addEventListener('change', function() {
      d.noUiSlider.set(this.value);
    }),
    e(d, '50'));
  var u = document.getElementById('count-phones-slider'),
    p = document.getElementById('count-phones-input');
  u &&
    (noUiSlider.create(u, {
      tooltips: [!0],
      format: wNumb({ decimals: 0 }),
      pips: { mode: 'values', values: [0, 30], density: 50 },
      start: [15],
      step: 1,
      range: { min: [0], max: [30] }
    }),
    u.noUiSlider.on('update', function(t, a) {
      $(p)
        .val(t[a])
        .change(),
        e(u);
    }),
    p.addEventListener('change', function() {
      u.noUiSlider.set(this.value);
    }),
    e(d, '50')),
    $('.js-prices-input').change(function() {
      console.log('Расчет!'), calculate();
    });
  var h = { 1: 1e3, 2: 1500, 3: 1e3, 4: 2400, 5: 1800, 6: 7600 },
    m = { 1: 300, 2: 350, 3: 300, 4: 700, 5: 500, 6: 1900 },
    v = { 1: 0, 2: 0, 3: 0, 4: 2500, 5: 0, 6: 0 },
    f = { 1: 0.7, 2: 0.9, 3: 0.7, 4: 1.55, 5: 1.2, 6: 4.2 },
    g = !0;
  (calculate = function() {
    var e = parseInt(
        $('#count-session-input')
          .val()
          .replace(/\D/g, '')
      ),
      a = parseInt(
        $('#length-session-input')
          .val()
          .replace(/\D/g, '')
      ),
      s = parseInt(
        $('#count-phones-input')
          .val()
          .replace(/\D/g, '')
      ),
      i = parseInt($('.js-prices-region:checked').val()),
      n = e * a;
    3 > a && (a = 3);
    var o = Math.round(((a / 7.79 / 4.54) * e) / 12);
    4 > o && (o = 4);
    var r = (o * v[i], Math.max(n * f[i], h[i]) + s * m[i]),
      l =
        Math.floor(
          100 *
            (1350 > n ? 3900 : 15e3 > n ? n * (3.063 - 111e-6 * n) : 95e3 > n ? n * (1.5787 - 916e-8 * n) : 0.707 * n)
        ) /
          100 +
        300 * s;
    $('#prices-summ-advance').text('0'),
      (l = 100 * Math.floor(l / 100)),
      (r = 10 * Math.floor(r / 10)),
      $('#prices-summ-program').text(t(l)),
      $('#prices-summ-connect').text(t(r)),
      $('#prices-summ-total').text(t(r + l));
  }),
    $('.prices').length && calculate(),
    setTimeout(function() {
      g = !1;
    }, 1e3),
    $('.js-validate').each(function() {
      $(this).validate({
        focusInvalid: !1,
        rules: {
          name: { required: !0 },
          mail: { required: !0, email: !0 },
          phon: { required: !0 },
          pasw: { required: !0 }
        },
        messages: { name: '', mail: '', phon: '', pasw: '' },
        errorClass: 'input-error',
        highlight: function(e, t, a) {
          $(e)
            .parent()
            .addClass(t);
        },
        unhighlight: function(e, t, a) {
          $(e)
            .parent()
            .removeClass(t);
        },
        submitHandler: function(e) {
          $(e).addClass('is-submitted'), alert('Submitted!');
        }
      });
    }),
    $("[name='phon']").mask('+7 (999) 999-99-99', { placeholder: '_' }),
    a('.js-load-events', '#js-for-load-events', 'ajax/more_events.php'),
    $('.events__nav-btn').click(function(e) {
      var t = $(this);
      t.hasClass('is-active') && (e.stopPropagation(), e.preventDefault(), t.parent().toggleClass('is-open'));
    }),
    $(document).click(function(e) {
      var t = $('.events__nav-btn');
      t.is(e.target) || 0 !== t.has(e.target).length || $('.events__nav').removeClass('is-open');
    }),
    $('.js-show-popup').click(function() {
      var e = '#' + $(this).data('pop');
      $(e).addClass('is-active'), $('body').addClass('mod-is-cutted');
    }),
    $(document).on('click', '.popup *', function(e) {
      e.stopPropagation();
    }),
    $(document).on('click', '.popup__close, .popup', function() {
      $('.popup').removeClass('is-active'), $('body').removeClass('mod-is-cutted');
    });
}),
  $(document).bind('mouseup touchend', function(e) {
    var t = $('.header__mobile, .menu-bugrer');
    t.is(e.target) ||
      0 !== t.has(e.target).length ||
      ($('.header__mobile, .menu-bugrer').removeClass('active'), $('body').removeClass('overflow'));
  }),
  $(function() {
    $('.apss-sidebar-mobile-close').click(function() {
      $('.apss-social-share-sidebar-mobile').hide();
    });

    $.fn.inputField = function() {
      var $input = $(this).children('input');

      $input.each(function(index, element) {
        var $that = $(element);
        if ($that.val().trim() !== '') {
          $that.parent().addClass('has-text');
        }
      });
      $input.focusin(function() {
        $(this)
          .parent()
          .addClass('has-text');
      });
      $input.focusout(function() {
        var $that = $(this),
          timer;
        timer = setTimeout(function() {
          if ($that.val().trim() === '') {
            $that.parent().removeClass('has-text');
            clearTimeout(timer);
          }
        }, 100);
      });
      return this;
    };
    //Инициализация работы плейсхолдеров в инпутах
    $('.js-input-field').inputField();

    $(document).on('click', 'a[href*="www.calltouch.ru"], a[href*="https://www.calltouch.ru"]', function() {
      ga('send', 'event', 'blog', 'website');
    });
  });
