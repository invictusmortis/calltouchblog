<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package OnePress
 */

get_header();
?>

<?php if(is_category() || is_front_page()):?>
	<?php get_template_part( 'template-parts/list' );?>
<?php elseif(is_page()): ?>
	<?php get_template_part( 'template-parts/page' );?>
<?php elseif(is_single()):?>
	<?php get_template_part( 'template-parts/article' );?>
<?php endif;?>

<?php get_footer(); ?>