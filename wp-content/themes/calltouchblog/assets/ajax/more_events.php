<?php  
  header("Content-type: text/html, charset=utf-8");
  header("Cache-Control: no-store, no-cache, must-revalidate");
  header("Cache-Control: post-check=0, pre-check=0", false);
  header("Pragma: no-cache");
?>

<div class="events-list__unit is-finished just-loaded">
    <div class="events-list__unit-status">
        <i class="icon-finish"></i>
    </div>
    <div class="events-list__unit-inner">
        <div class="events-list__unit-pict">
            <a class="events-list__unit-link" href="#" target="_blank">
                <img class="events-list__unit-img" src="img/events/event_05_680x265.jpg" alt="Оффлайн-конференция о принципах работы с маркетингом в веб-проектах">
            </a>
        </div>
        <div class="events-list__unit-text">
            <div class="events-list__unit-type">
                <a href="#" title="Показать такие мероприятия">Call-day</a>
            </div>
            <h3 class="events-list__unit-title">Оффлайн-конференция о принципах работы с маркетингом в веб-проектах</h3>
            <div class="events-list__unit-note">
                Описание вебинара, его особенностей, специфики и тех
                преимуществ, которые должны повлиять на пользователя.
            </div>
        </div>
        <div class="events-list__unit-bttm">
            <ul class="events-list__unit-info">
                <li><i class="icon-finish"></i>12 апреля 2016</li>
                <li><i class="icon-point-gray"></i>ТЦ "Корона", г. Москва, пр. Красноармейцев, 15</li>
            </ul>
            <a class="events-list__unit-more" href="#" target="_blank">Ссылка откроется в новом окне<i class="icon-window"></i></a>
        </div>
    </div>
</div>

<div class="events-list__unit is-finished just-loaded">
    <div class="events-list__unit-status">
        <i class="icon-finish"></i>
    </div>
    <div class="events-list__unit-inner">
        <div class="events-list__unit-pict">
            <a class="events-list__unit-link" href="#" target="_blank">
                <img class="events-list__unit-img" src="img/events/event_06_680x265.jpg" alt="Как заработать миллионы на личном сайте">
            </a>
        </div>
        <div class="events-list__unit-text">
            <div class="events-list__unit-type">
                <a href="#" title="Показать такие мероприятия">Call-day</a>
            </div>
            <h3 class="events-list__unit-title">Как заработать миллионы на личном сайте</h3>
            <div class="events-list__unit-note">
                Описание вебинара, его особенностей, специфики и тех
                преимуществ, которые должны повлиять на пользователя.
            </div>
        </div>
        <div class="events-list__unit-bttm">
            <ul class="events-list__unit-info">
                <li><i class="icon-finish"></i>12 апреля 2016</li>
                <li><i class="icon-point-gray"></i>ТЦ "Корона", г. Москва, пр. Красноармейцев, 15</li>
            </ul>
            <a class="events-list__unit-more" href="#" target="_blank">Ссылка откроется в новом окне<i class="icon-window"></i></a>
        </div>
    </div>
</div>

<div class="events-list__unit is-finished just-loaded">
    <div class="events-list__unit-status">
        <i class="icon-finish"></i>
    </div>
    <div class="events-list__unit-inner">
        <div class="events-list__unit-pict">
            <a class="events-list__unit-link" href="#" target="_blank">
                <img class="events-list__unit-img" src="img/events/event_05_680x265.jpg" alt="Оффлайн-конференция о принципах работы с маркетингом в веб-проектах">
            </a>
        </div>
        <div class="events-list__unit-text">
            <div class="events-list__unit-type">
                <a href="#" title="Показать такие мероприятия">Call-day</a>
            </div>
            <h3 class="events-list__unit-title">Оффлайн-конференция о принципах работы с маркетингом в веб-проектах</h3>
            <div class="events-list__unit-note">
                Описание вебинара, его особенностей, специфики и тех
                преимуществ, которые должны повлиять на пользователя.
            </div>
        </div>
        <div class="events-list__unit-bttm">
            <ul class="events-list__unit-info">
                <li><i class="icon-finish"></i>12 апреля 2016</li>
                <li><i class="icon-point-gray"></i>ТЦ "Корона", г. Москва, пр. Красноармейцев, 15</li>
            </ul>
            <a class="events-list__unit-more" href="#" target="_blank">Ссылка откроется в новом окне<i class="icon-window"></i></a>
        </div>
    </div>
</div>

<div class="events-list__unit is-finished just-loaded">
    <div class="events-list__unit-status">
        <i class="icon-finish"></i>
    </div>
    <div class="events-list__unit-inner">
        <div class="events-list__unit-pict">
            <a class="events-list__unit-link" href="#" target="_blank">
                <img class="events-list__unit-img" src="img/events/event_06_680x265.jpg" alt="Как заработать миллионы на личном сайте">
            </a>
        </div>
        <div class="events-list__unit-text">
            <div class="events-list__unit-type">
                <a href="#" title="Показать такие мероприятия">Call-day</a>
            </div>
            <h3 class="events-list__unit-title">Как заработать миллионы на личном сайте</h3>
            <div class="events-list__unit-note">
                Описание вебинара, его особенностей, специфики и тех
                преимуществ, которые должны повлиять на пользователя.
            </div>
        </div>
        <div class="events-list__unit-bttm">
            <ul class="events-list__unit-info">
                <li><i class="icon-finish"></i>12 апреля 2016</li>
                <li><i class="icon-point-gray"></i>ТЦ "Корона", г. Москва, пр. Красноармейцев, 15</li>
            </ul>
            <a class="events-list__unit-more" href="#" target="_blank">Ссылка откроется в новом окне<i class="icon-window"></i></a>
        </div>
    </div>
</div>
