
<!-- PAGE-BODY -->
<div class="page__body">
	<div class="article">
		<div class="container">
			<div class="g-row">

				<!-- ARTICLE BODY -->
				<div class="article__body">
					<!-- USER_CONTENT -->
					<div class="user-content-styles">
					<?=get_the_content()?>
					</div>
					<!-- /USER_CONTENT -->
				</div>
				<!-- /ARTICLE BODY -->

				<?php get_template_part("section-parts/right-list-posts");?>

			</div>
		</div>
	</div>
</div>
<!-- /PAGE-BODY -->

<?php get_template_part("section-parts/blog-form");?>