
<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package OnePress
 */

get_header();
?>


<div class="page__body">
	<div class="container">

		<!-- ARTICLES -->
		<div class="articles">
				<?php
                if($obPosts = searchByTag($_GET["s"]) ) {
                    foreach ($obPosts as $post) { ?>

                        <?php

                        /*if($_SERVER["REMOTE_ADDR"] == "217.144.187.243") {
                            echo "<pre>";
                            print_r($post);
                            die;
                        }*/

                        $thumbnail = get_the_post_thumbnail($post->ID, "medium", array(
                            'class' => "articles-unit__thumb-img",
                            'alt' => trim(strip_tags($post->post_title)),
                            'title' => trim(strip_tags($post->post_title)),
                        ));

                        ?>

                        <div class="articles-unit">
                            <div class="articles-unit__thumb">
                                <a class="articles-unit__thumb-link" href="<?= the_permalink() ?>">
                                    <?= $thumbnail ?>
                                </a>
                            </div>
                            <div class="articles-unit__details">
                                <div class="articles-unit__text">
                                    <div class="articles-unit__date">
                                        <?php
                                        if ($obCategory = get_the_category($post->ID)) {
                                            $sCategory = "&nbsp;&nbsp;|&nbsp;&nbsp;";
                                            $sCategory .= "<a href='/" . $obCategory[0]->taxonomy . "/" . $obCategory[0]->slug . "/'>" . $obCategory[0]->name . "</a>&nbsp;&nbsp;";
                                        }
                                        ?>
                                        <?= get_the_date('d F Y', $post->ID); ?><?= $sCategory; ?>
                                        <?php

                                        if ($sView = get_post_meta($post->ID, "views", true)) {

                                            ?>
                                            |&nbsp;&nbsp;
                                            <div class="eye"><?= $sView; ?></div>
                                        <?php } ?>
                                    </div>

                                    <div class="articles-unit__title">
                                        <a href="<?= the_permalink() ?>"><?= trim(strip_tags($post->post_title)) ?></a>
                                    </div>

                                    <div class="articles-unit__desc">
                                        <a href="<?= the_permalink() ?>"><?= trim(strip_tags($post->post_excerpt)) ?></a>
                                    </div>

                                </div>


                                <?php
                                $thisCats = get_the_category();
                                $sTags = "";
                                if ($obTags = wp_get_post_tags($post->ID)) {
                                    foreach ($obTags as $obTag) {
                                        $sTags .= $obTag->name . ", ";
                                    }
                                    $sTags = substr($sTags, 0, -2);
                                }
                                ?>

                                <?php if (count($obTags)): ?>

                                    <ul class="tags-list articles-unit__links">
                                        <?php
                                        foreach ($obTags as $cat) {
                                            ?>
                                            <li>
                                                <a href="/?s=<?=urlencode($cat->name);?>&id=<?=$cat->term_id;?>"><?=$cat->name;?></a>
                                            </li>
                                        <?php } ?>
                                    </ul>

                                <?php endif; ?>

                            </div>
                        </div>

                    <?php } ?>


                <?php
                } else {
                    emptyResult();
                }
                ?>
		</div>



		<!-- ARTICLES -->
	</div>

</div>
<!-- /PAGE-BODY -->

<?php get_footer(); ?>