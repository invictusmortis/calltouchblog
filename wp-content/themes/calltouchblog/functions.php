<?php
add_theme_support('post-thumbnails'); // для всех типов постов

add_action('wp_enqueue_scripts', '_s_scripts');
function _s_scripts()
{

	// Loads required CSS header only.
	wp_enqueue_style('_s-style', get_stylesheet_uri());

	// Loads bundled frontend CSS.
	wp_enqueue_style('_s-frontend-styles', get_stylesheet_directory_uri() . '/public/frontend.css');

	wp_enqueue_script('_s-frontend-scripts', get_template_directory_uri() . '/public/frontend-bundle.js', array(), null, true);

	if (is_singular() && comments_open() && get_option('thread_comments')) {
		wp_enqueue_script('comment-reply');
	}
}


function breadcrumbs()
{

	echo '<ul class="breadcrumbs page__head-brcr" itemscope itemtype="http://schema.org/BreadcrumbList">';

	if (!is_front_page()) {

		echo '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
				<a itemprop="item" href="https://www.calltouch.ru/"><span itemprop="name">Главная</span></a>
				<meta itemprop="position" content="1" />
			</li>';
		echo '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
				<a itemprop="item" href="/"><span itemprop="name">Блог</span></a>
				<meta itemprop="position" content="2" />
			</li>';

		if (is_category()) {
			$cat = get_queried_object();
			echo "<li><span>" . $cat->name . "</span></li>";
		}

		if (is_single()) {

			$cat = get_the_category();
			$title = get_the_title();

			echo '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
				<a itemprop="item" href="/category/' . $cat[0]->slug . '"><span itemprop="name">' . $cat[0]->name . '</span></a>
				<meta itemprop="position" content="3"/>
			</li>';

			echo "<li><span>" . $title  . "</span></li>";
		}

		if (is_page()) {
			$title = get_the_title();
			echo "<li><span>" . $title  . "</span></li>";
		}
	} else {
		echo '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
				<a itemprop="item" href="https://www.calltouch.ru/"><span itemprop="name">Главная</span></a>
				<meta itemprop="position" content="1" />
			</li>';
		echo "<li><span>Блог</span></li>";
	}

	echo "</ul>";
}


// numbered pagination
function pagination($pages = '', $range = 4)
{
	$showitems = ($range * 2) + 1;

	global $paged;
	if (empty($paged)) $paged = 1;

	if ($pages == '') {
		global $wp_query;
		$pages = $wp_query->max_num_pages;
		if (!$pages) {
			$pages = 1;
		}
	}


	if (1 != $pages) {

		echo '<ul class="pagination page__pagination">';

		if ($paged > 1) {
			echo '<li><a class="pagination__prev" href="' . get_pagenum_link($paged - 1) . '">prev</a></li>';
		}


		if ($paged > 5) {
			echo '<li><a class="pagination__page" href="' . get_pagenum_link(1) . '">1</a></li>';
			echo '<li><b class="pagination__dots">...</b></li>';
		}

		for ($i = 1; $i <= $pages; $i++) {

			if (1 != $pages && (!($i >= $paged + $range + 1 || $i <= $paged - $range - 1) || $pages <= $showitems)) {

				if ($i == $paged) {
					echo '<li class="is-active"><a class="pagination__page" href="' . get_pagenum_link($i) . '">' . $i . '</a></li>';
				} else {
					echo '<li><a class="pagination__page" href="' . get_pagenum_link($i) . '">' . $i . '</a></li>';
				}
			}
		}

		if ($pages - $paged > 4) {
			echo '<li><b class="pagination__dots">...</b></li>';
			echo '<li><a class="pagination__page" href="' . get_pagenum_link($pages) . '">' . $pages . '</a></li>';
		}


		if ($paged < $pages) {
			echo '<li><a class="pagination__next" href=' . get_pagenum_link($paged + 1) . '>next</a></li>';
		}

		echo '</ul>';
	}
}

function emptyResult()
{
	echo "<div style=\"margin-bottom: 70px;\">";
	echo "<div>Результатов не найдено...</div>";
	echo "</div>";
}

function show404()
{
	status_header(404);
	nocache_headers();
	include(get_query_template('404'));
}

function ppr($arArray)
{
	if ($_COOKIE["test"]) {
		echo "<pre>";
		print_r($arArray);
		echo "</pre>";
	}
}

function searchByTag($sTag)
{
	$product_cats = 'books, renew, sanctuary';
	$args = array(
		'posts_per_page' => -1,
		'post_type' => 'post',
		'orderby' => 'date',
		'order' => 'DESC',
		's' => $sTag
	);
	$obPosts = new WP_Query($args);

	if ($obPosts->have_posts()) return $obPosts->posts;
}
