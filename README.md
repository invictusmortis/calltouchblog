# Calltouch blog

### Installation

Execute from docker-compose file, install all the dependencies and devDependencies and start the server.

```sh
$ docker-compose up -d
$ cd wp-content/themes/calltouchblog/
$ npm i
$ npm dev
```

### Build

```sh
$ cd wp-content/themes/calltouchblog/
$ npm build
```

### Format and lint

```sh
$ cd wp-content/themes/calltouchblog/
$ npm eslint
$ npm eslint:fix
$ npm format
$ npm stylelint
$ stylelint:fix
```

Thats all...
